define(['services', 'filters'], function(Services, Filters) {
    return Backbone.Model.extend({
        initialize: function () {
            var model = this;
            if (!this.priceMultiplier) {
                this.priceMultiplier = function () {
                    return model.attributes.quantity;
                };
            }
            this.calculateCartItemProperties();
        },
        calculateCartItemProperties: function () {
            var product = this.attributes.product;
            var priceBeforeDiscounts = product.attributes.price * this.priceMultiplier();
            var discount = product.discount(this.attributes.quantity);
            var finalPrice = priceBeforeDiscounts - discount;

            this.set({
                formattedPrice: Filters.currency(priceBeforeDiscounts),
                finalPrice: finalPrice,
                formattedDiscount: Filters.currency(discount),
                formattedFinalPrice: Filters.currency(finalPrice)
            });
        },
        increaseQuantity: function (quantityToIncreaseBy) {
            var oldQuantity = this.attributes.quantity;
            this.set({quantity: oldQuantity + quantityToIncreaseBy});
            this.calculateCartItemProperties();
        }
    })
});