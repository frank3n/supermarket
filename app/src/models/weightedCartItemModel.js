define(['services', 'filters', 'models/cartItemModel'], function(Services, Filters, CartItemModel) {
    return CartItemModel.extend({
        initialize: function(attributes, options) {
            this.priceMultiplier = function() {
                return (this.attributes.quantity / this.attributes.product.attributes.pricePerWeightUnit).toFixed(2);
            };

            CartItemModel.prototype.initialize.call(this, attributes, options);
        }
    });
});