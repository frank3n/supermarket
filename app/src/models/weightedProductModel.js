define(['services', 'filters', 'models/productModel'], function(Services, Filters, ProductModel) {
    return ProductModel.extend({
        defaults: {
          pricePerWeightUnit: 100
        },
        initialize: function(attributes) {
            this.set({
                formattedPrice: Filters.currency(attributes.price) + ' per '+ this.attributes.pricePerWeightUnit +'g'
            });
        }
    });
});