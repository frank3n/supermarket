define(['models/cartItemModel'], function(CartItemModel) {
    return Backbone.Collection.extend({
        model: CartItemModel,
        add: function(item) {
            var existingCartItem = this.get(item.id);
            if(existingCartItem) {
                existingCartItem.increaseQuantity(item.attributes.quantity);
            } else {
                Backbone.Collection.prototype.add.call(this, item);
            }
        }
    });
});