define(['models/productModel'], function(ProductModel) {
    return Backbone.Collection.extend({
        model: ProductModel
    });
});