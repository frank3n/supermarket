define(['services', 'filters'], function(Services, Filters) {
    return Backbone.Model.extend({
        defaults: {
            title: '',
            price: 0
        },
        discount: function() {
            return 0;
        },
        initialize: function(attributes) {
            if(attributes.discount != null) this.discount = attributes.discount;
            this.set({
                formattedPrice: Filters.currency(attributes.price)
            });
        }
    });
});