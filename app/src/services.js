define(['models/productListModel', 'models/productModel', 'models/weightedProductModel', 'models/cartItemsModel'],
    function(ProductListModel, ProductModel, WightedProductModel, CartItemsModel) {
        return {
            ProductsList: (function() {
                var bread = new ProductModel({ id: 0, title: 'Bread', price: 125 });
                var apple = new ProductModel({
                    id: 1,
                    title: 'Apple',
                    price: 40,
                    discount: function(quantity) {
                        var discountValue = 20;
                        var howManyTimesDiscountApplies = Math.floor(quantity / 3);
                        return howManyTimesDiscountApplies * discountValue;
                    }
                });
                var fizzyDrink = new ProductModel({
                    id: 2,
                    title: 'Fizzy Drink',
                    price: 70,
                    discount: function(quantity) {
                        var discountValue = this.attributes.price;
                        var howManyTimesDiscountApplies = Math.floor(quantity / 3);
                        return howManyTimesDiscountApplies * discountValue;
                    }
                });
                var cheese = new WightedProductModel({
                    id: 3,
                    title: 'Cheese',
                    price: 199
                });

                return new ProductListModel([ bread, apple, fizzyDrink, cheese ]);
            })(),
            CartItems: new CartItemsModel()
        };
});