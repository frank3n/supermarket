requirejs.config({
    baseUrl: './',
    paths: {
        models: './src/models',
        views: './src/views',
        templates: './src/templates',
        text: './libs/text',
        backbone: './libs/backbone',
        jquery: './libs/jquery',
        underscore: './libs/underscore',
        services: './src/services',
        filters: './src/filters'
    }
});

requirejs(['jquery', 'underscore', 'backbone']);

require(['views/productListView', 'views/cartItemsView', 'views/cartSummaryView'], function(ProductListView, CartItemsView, CartSummaryView) {
    $(function() {
        new ProductListView().render();
        new CartItemsView();
        new CartSummaryView();
    });
});