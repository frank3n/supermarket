define(['views/productView', 'services'], function(ProductView, Services) {
    return Backbone.View.extend({
        el: '#products > ul',
        productsList: Services.ProductsList,
        render: function() {
            var that = this;
            $(this.el).empty();

            this.productsList.each(function(product) {
                $(that.el).append(new ProductView({ model: product }).render().el);
            });
            return this;
        }
    });
});