define(['text!templates/cartItem.html', 'services'], function(Template, Services) {
    return Backbone.View.extend({
        tagName: 'li',
        className: 'cart-item',
        template: _.template(Template),
        events: {
            'click .remove': 'removeFromCart'
        },
        render: function() {
            this.$el.html( this.template( this.model.attributes ) );
            return this;
        },
        removeFromCart: function() {
            Services.CartItems.remove(this.model);
        }
    });
});