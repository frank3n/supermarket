define(['views/cartItemView', 'services'], function(CartItemView, Services) {
    var cartItems = Services.CartItems;

    return Backbone.View.extend({
        el: '#cart > ul',
        initialize: function() {
            this.listenTo(cartItems, 'add', this.render);
            this.listenTo(cartItems, 'remove', this.render);
            this.listenTo(cartItems, 'change:finalPrice', this.render);
            this.listenTo(cartItems, 'change:quantity', this.render);
        },
        render: function() {
            var that = this;
            $(this.el).empty();

            cartItems.each(function(item) {
                $(that.el).append(new CartItemView({ model: item }).render().el);
            });
            return this;
        }
    });
});