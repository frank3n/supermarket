define(['text!templates/product.html', 'services', 'models/cartItemModel', 'models/weightedCartItemModel'],
    function(Template, Services, CartItemModel, WeightedCartItemModel) {
        var CartItemModelFactory = function(product) {
            return product.attributes.pricePerWeightUnit ? WeightedCartItemModel : CartItemModel;
        };

        return Backbone.View.extend({
            tagName: 'li',
            className: 'product',
            template: _.template(Template),
            events: {
                'click .add': 'addToCart'
            },
            render: function() {
                this.$el.html(this.template(this.model.attributes));
                this.$quantity = this.$('.quantity');
                return this;
            },
            addToCart: function() {
                if(!parseInt(this.$quantity.val())) return;

                var product = this.model;
                var CartItemModel = CartItemModelFactory(product);

                Services.CartItems.add(new CartItemModel({
                    id: product.id,
                    product: product,
                    quantity: parseInt(this.$quantity.val())
                }));
            }
        });
});