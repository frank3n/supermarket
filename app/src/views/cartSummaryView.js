define(['text!templates/cartSummary.html', 'services', 'filters'],
    function(Template, Services, Filters) {
        var cartItems = Services.CartItems;

        return Backbone.View.extend({
            el: '#cart .summary',
            template: _.template(Template),
            initialize: function() {
                this.listenTo(cartItems, 'add', this.render);
                this.listenTo(cartItems, 'remove', this.render);
                this.listenTo(cartItems, 'change:finalPrice', this.render);
            },
            render: function() {
                var totalToPay = cartItems.reduce(function(totalPrice, cartItem) {
                    return totalPrice + cartItem.attributes.finalPrice;
                }, 0);
                this.$el.html(this.template({ totalToPay: Filters.currency(totalToPay) }));
                return this;
            }
        });
});