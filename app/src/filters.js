define({
    currency: function(price) {
        return (price / 100).toFixed(2) + '£'
    }
});