var Page = {
    waitForProductsListToBeInitialised: function() {
        casper.waitForSelector('#products > ul > li');
    },
    addProductByIndexToCart: function(index, quantity) {
        var setProductQuantity = function() {
            casper.sendKeys('#products > ul > li:nth-child(' + index + ') > div > input', quantity.toString(), {
                reset: true
            });
        };
        var addProductToCart = function() {
            casper.click('#products > ul > li:nth-child(' + index + ') > div > button');
        };
        setProductQuantity();
        addProductToCart();
    },
    addBreadToCart: function(quantity) {
        this.addProductByIndexToCart(1, quantity);
    },
    addAppleToCart: function(quantity) {
        this.addProductByIndexToCart(2, quantity);
    },
    addFizzyDrinkToCart: function(quantity) {
        this.addProductByIndexToCart(3, quantity);
    },
    addCheeseToCart: function(quantity) {
        this.addProductByIndexToCart(4, quantity);
    },
    removeItemFromCartByIndex: function(cartItemIndex) {
        casper.click('#cart > ul > li:nth-child('+cartItemIndex+') > div > button');
    }
};

var Assert = {
  totalPrice: function(test, totalPrice) {
      test.assertEvalEquals(function() {
          return __utils__.findOne('#cart > div > span').textContent;
      }, 'Total to pay: '+totalPrice+'£');
  }
};

casper.test.setUp(function() {
    casper.start('http://localhost:8080/index.html');
    Page.waitForProductsListToBeInitialised();
});

casper.test.begin('Should display Cart header', function(test){
    casper.then(function(){
        test.assertEvalEquals(function() {
            return __utils__.findOne('#cart > h2').textContent;
        }, 'Cart');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should display products list', function(test){
    casper.then(function(){
        test.assertEvalEquals(function() {
            var productLabels = __utils__.findAll('#products > ul > li > label');

            return productLabels.map(function(productLabel) {
                return productLabel.textContent;
            });
        }, ['Bread 1.25£', 'Apple 0.40£','Fizzy Drink 0.70£','Cheese 1.99£ per 100g']);

        test.assertElementCount('#products > ul > li > div > input', 4);
        test.assertElementCount('#products > ul > li > div > button', 4);
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should add bread to cart', function(test){
    casper.then(function(){
        Page.addBreadToCart(1);

        test.assertEvalEquals(function() {
             return __utils__.findOne('#cart > ul > li:nth-child(1) > label').textContent;
        }, 'Bread 1.25£');

        test.assertEvalEquals(function() {
            return __utils__.findOne('#cart > ul > li:nth-child(1) > div > p:nth-child(4)').textContent;
        }, 'Final price: 1.25£');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should display total price', function(test){
    casper.then(function(){
        Page.addBreadToCart(2);
        Assert.totalPrice(test, '2.50');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should not apply discount for less than 3 apples', function(test){
    casper.then(function(){
        Page.addAppleToCart(2);
        Assert.totalPrice(test, '0.80');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should correctly apply apples discount for more than 3 apples', function(test){
    casper.then(function(){
        Page.addAppleToCart(10);
        Assert.totalPrice(test, '3.40');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should not apply discount for less than 3 fizzy drinks', function(test){
    casper.then(function(){
        Page.addFizzyDrinkToCart(2);
        Assert.totalPrice(test, '1.40');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should apply fizzy drinks discount', function(test){
    casper.then(function(){
        Page.addFizzyDrinkToCart(10);
        Assert.totalPrice(test, '4.90');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should correctly appraise cheese for more than 100g', function(test){
    casper.then(function(){
        Page.addCheeseToCart(200);
        Assert.totalPrice(test, '3.98');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should correctly appraise cheese for less than 100g', function(test){
    casper.then(function(){
        Page.addCheeseToCart(10);
        Assert.totalPrice(test, '0.20');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should correctly sum items in the cart', function(test){
    casper.then(function(){
        Page.addCheeseToCart(10);
        Page.addFizzyDrinkToCart(10);
        Page.addAppleToCart(10);
        Page.addBreadToCart(2);
        Assert.totalPrice(test, '11.00');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should update total price after removing items', function(test){
    casper.then(function(){
        Page.addCheeseToCart(10);
        Page.addFizzyDrinkToCart(10);
        Page.removeItemFromCartByIndex(2);
        Assert.totalPrice(test, '0.20');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should apply fizzy drinks when adding drinks one by one', function(test){
    casper.then(function(){
        Page.addFizzyDrinkToCart(1);
        Page.addFizzyDrinkToCart(1);
        Page.addFizzyDrinkToCart(1);
        Assert.totalPrice(test, '1.40');
    });

    casper.run(function(){
        test.done();
    })
});

casper.test.begin('Should aggregate cheese', function(test){
    casper.then(function(){
        Page.addCheeseToCart(50);
        Page.addCheeseToCart(50);
        Assert.totalPrice(test, '1.99');
    });

    casper.run(function(){
        test.done();
    })
});

