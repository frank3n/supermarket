module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-connect');
    grunt.loadNpmTasks('grunt-casper');

    grunt.initConfig({
        connect: {
            server: {
                port: 8080,
                base: './app'
            }
        },
        casper: {
            test: {
                options: {
                    verbose: true,
                    test: true,
                    logLevel: 'debug'
                },
                files: {
                    src: ['test/**/*.js']
                }
            }
        }
    });
};